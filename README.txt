These games are intended to be run on an Arduino Esplora with a TFT screen installed. Some sketches have a menu built in to them. In the future, I hope to integrate multiple games
into a single sketch.

All sketches are released under CC BY-SA, allowing remixes, commercial, and personal use with atribution and shared similarly.

To power the Esplora using a LiPo battery:
Hardware needed: 
-1 battery (I use a 2500 mAH LiPo pack from Adafruit)
-1 5V Power boost module 
-1 LiPo charger (the power boost module and charger are avaliable on a single board from Adafruit, Powerboost 1000C)
-1 switch (if using the Powerboost 1000C power can be switched at logic level, meaning a low amp switch is acceptable)
-Wire

Connect your power boost module and charger if using seperate boards.
Connect the 5V power from your power boost module to the leftmost tinker kit connection (out B) on the Esplora, positive to left pin (out) and negative to right pin (d11).
	I decided to remove the tinker kit connectors and solder directly to the arduino to reduce profile.
Connect your power switch. While this will vary depending on the hardware used, with the Powerboost 1000C simply solder and switch to EN and GND. 
	If you need to switch power directly (ie, in line with the battery leads) a 2A+ switch is needed.
Be sure to disconnect the battery while uploading sketches. 

UPDATE 14APR2017: Initial push, uploaded README and Brick Breaker game.