/*

  Esplora TFT Brick Breaker

  This code is a derivative of the Arduino EsploraTFT pong.

  Original code:
  Released under CC0
  Created by Tom Igoe December 2012
  Modified 15 April 2013 by Scott Fitzgerald
  http://www.arduino.cc/en/Tutorial/EsploraTFTPong

  Derivative code:
  Released under CC BY-SA
  Written by Nick Biederman April 2017.
  

*/

#include <Esplora.h>
#include <TFT.h>            // Arduino LCD library
#include <SPI.h>

// variables for the position of the ball and paddle
int paddleX = 0;
int paddleY = 20;
int oldPaddleX, oldPaddleY;
int ballDirectionX = 1;
int ballDirectionY = 1;
int count = 0;
int gameChoice = 0;
int sound = 1;

int ballX = 25, ballY = 25, oldBallX = 25, oldBallY = 25;

int top[8] = {15, 15, 15, 15, 15, 15, 15, 15};

void setup() {

  Serial.begin(9600);

  // initialize the display
  EsploraTFT.begin();
  // set the background the black
  EsploraTFT.background(0, 0, 0);

  // game menu, for later implimentation of more games
  EsploraTFT.setTextSize(1);
  EsploraTFT.stroke(255, 255, 255);
  EsploraTFT.text("1. Brick Breaker", 5, 5);
  EsploraTFT.noStroke();
  while (gameChoice == 0) {
    if (Esplora.readButton(SWITCH_1) == LOW)
      gameChoice = 1;
    else
      gameChoice = 0;
  }

  if (gameChoice == 1) {
    //clear background and draw blocks at top
    EsploraTFT.background(0, 0, 0);
    EsploraTFT.fill(255, 150, 150);
    EsploraTFT.rect(0, 0, EsploraTFT.width(), 15);
  }
}

void loop() {
  //in big if statement for menu choices (prpbably should make switch)
  if (gameChoice == 1) {
    // save the width and height of the screen
    int myWidth = EsploraTFT.width();
    int myHeight = EsploraTFT.height();

    //turn beep on or off
    if (Esplora.readButton(SWITCH_1) == LOW) {
      sound = -sound;
      delay(5);
    }

    // map the paddle's location to the joystick's position
    paddleX = map(Esplora.readJoystickX(), 512, -512, 0, myWidth) - 20 / 2;
    paddleY = myHeight - 20;  //changed to be constant
    Serial.print(paddleX);
    Serial.print(" ");
    Serial.println(paddleY);

    // set the fill color to black and erase the previous
    // position of the paddle if different from present
    EsploraTFT.fill(0, 0, 0);

    if (oldPaddleX != paddleX || oldPaddleY != paddleY) {
      EsploraTFT.rect(oldPaddleX, paddleY, 20, 5);
    }

    // draw the paddle on screen, save the current position
    // as the previous.
    EsploraTFT.fill(255, 255, 255);
    EsploraTFT.rect(paddleX, paddleY, 20, 5);
    oldPaddleX = paddleX;
    oldPaddleY = paddleY;

    // read the slider to determinde the speed of the ball
    int ballSpeed = map(Esplora.readSlider(), 0, 1023, 0, 80) + 1;
    if (millis() % ballSpeed < 2) {
      moveBall();
    }

    // endgame notification
    while (count == 21) {
      EsploraTFT.background(0, 0, 0);
      EsploraTFT.stroke(255, 255, 255);
      EsploraTFT.setTextSize(3);
      EsploraTFT.text("Game", 5, 20);
      EsploraTFT.text("Complete", 5, 44);
      delay(10000);
    }
  } // end of brick breaker code
}


// this function determines the ball's position on screen
void moveBall() {
  // if the ball goes offscreen, reverse the direction:
  if (ballX > EsploraTFT.width() || ballX < 0) {
    ballDirectionX = -ballDirectionX;
  }
  if (ballY > EsploraTFT.height() || ballY == 0)
    ballDirectionY = -ballDirectionY;

  //if ball hits block, reverse, delete block, and play sound if desired
  else {
    for (int i = 0; i < 7; i ++) {
      if ( ballY <= top[i] && ballX >= i * 23 && ballX <= (i * 23) + 23) {
        ballDirectionY = -ballDirectionY;
        top[i] = top[i] - 5;
        EsploraTFT.fill(0, 0, 0);
        EsploraTFT.rect(i * 23, top[i], 23, 5);
        ballY += ballDirectionY;
        count ++;
        if (sound == 1)
          Esplora.tone(2500, 80);
      }
    }
  }

  // check if the ball and the paddle occupy the same space on screen
  if (inPaddle(ballX, ballY, paddleX, paddleY, 20, 5)) {
    ballDirectionY = -ballDirectionY;
  }

  // update the ball's position
  ballX += ballDirectionX;
  ballY += ballDirectionY;

  // erase the ball's previous position
  EsploraTFT.fill(0, 0, 0);

  if (oldBallX != ballX || oldBallY != ballY) {
    EsploraTFT.rect(oldBallX, oldBallY, 5, 5);
  }

  // draw the ball's current position
  EsploraTFT.fill(255, 255, 255);

  EsploraTFT.rect(ballX, ballY, 5, 5);

  oldBallX = ballX;
  oldBallY = ballY;

}

// this function checks the position of the ball
// to see if it intersects with the paddle
boolean inPaddle(int x, int y, int rectX, int rectY, int rectWidth, int rectHeight) {
  boolean result = false;

  if ((x >= rectX && x <= (rectX + rectWidth)) &&
      (y >= rectY && y <= (rectY + rectHeight))) {
    result = true;
  }

  return result;
}
